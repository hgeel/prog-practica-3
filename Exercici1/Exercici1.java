package Exercici1;

import Keyboard.Keyboard;

public class Exercici1 {
	
	public static void main(String[] args) {
		
		System.out.println("Escriu una frase lletra per lletra. Acaba amb un punt (.): ");
		
		String frase = "";
		
		//Es llegeixen els caracters un a un.
		char input = ' ';
		while(input != '.') {
			input = Keyboard.readChar();
			frase += input;
		}
		
		String output = "";
		
		//Se li dona la volta a la frase.
		int n = frase.length() - 1;
		while(n >= 0) {
			output += frase.charAt(n);
			n--;
		}
		
		//S'imprimeix la frase.
		System.out.print("Resultat: ");
		System.err.println(output);
		
	}

}
