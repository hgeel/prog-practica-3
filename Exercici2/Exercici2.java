package Exercici2;

import Keyboard.Keyboard;

public class Exercici2 {
	
	public static void main(String[] args) {
		
		System.out.println("Escriu una frase lletra per lletra."
				+ "\nAcaba amb un punt (.) o quan s'hagin entrat 3 a's:");
		
		int as = 0;
		int novocals = 0;
		char c = ' ';
		while(c != '.' && as < 3) {
			c = Keyboard.readChar();
			if(c == 'a' || c == 'A') {
				as++;
			}
			if(c != 'a' && c != 'A' 
					&& c != 'e' && c != 'E' 
					&& c != 'i' && c != 'I' 
					&& c != 'o' && c != 'O' 
					&& c != 'u' && c != 'U') {
				
				novocals++;
				
			}
		}
		
		System.out.println();
		System.out.println("El nombre de caracters no vocals introdu�ts �s: " + novocals);
		
	}

}
