package Exercici4;

import Keyboard.Keyboard;

public class Exercici4 {
	
	public static void main(String[] args) {
		
		System.out.println("Escriu el nombre petit del interval:");
		
		int a = Keyboard.readInt();
		while(a < 0) {
			System.out.println("Incorrecte. El nombre petit ha de ser m�s gran que 0.");
			System.out.println("Escriu un altre:");
			a = Keyboard.readInt();
		}
		
		System.out.println("Escriu el nombre gran del interval.");
		int b = Keyboard.readInt();
		while(b < a) {
			System.out.println("Incorrecte. El nombre gran del interval ha de ser m�s gran que el nombre petit.");
			System.out.println("Escriu un altre:");
			b = Keyboard.readInt();
		}
		
		System.out.println("Dins del interval [" + a + "," + b + "]:");
		
		int i = a;
		while(i <= b) {
			if(i % 5 == 0) {
				String s_i = String.valueOf(i);
				String s_i_invertit = "";
				int n = s_i.length() - 1;
				while(n >= 0) {
					s_i_invertit += s_i.charAt(n);
					n--;
				}
				int invertit = Integer.valueOf(s_i_invertit);
				System.out.println("El invertit de " + i + " �s " + invertit + " i la seva arrel quadrada �s " + Math.sqrt(invertit) + ".");
			}
			i++;
		}
		
	}

}
