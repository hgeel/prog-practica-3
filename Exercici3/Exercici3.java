package Exercici3;

import Keyboard.Keyboard;

public class Exercici3 {
	
	public static void main(String[] args) {
		
		System.out.println("Invertir un numero positiu.");
		System.out.println("Escriu un numero: ");
		
		int i = Keyboard.readInt();
		while(i < 0) {
			System.out.println("Incorrecte. El nombre ha de ser positiu.");
			System.out.println("Escriu un altre numero: ");
			i = Keyboard.readInt();
		}
		
		String nombre = String.valueOf(i);
		String invertit = "";
		int n = nombre.length() - 1;
		while(n >= 0) {
			invertit += nombre.charAt(n);
			n--;
		}
		
		System.out.println("El nombre invertit �s: " + invertit);
		
	}

}
